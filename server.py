import socket
import json
import datetime

uptime = datetime.datetime.now()
help_ =  "uptime - returns time of server activity  \ninfo - returns version of the server and issue date \nhelp - returns list of commands \nstop - terminates connection"
info = "version 1.0.0, issue date 13.12.2022"
HOST = "127.10.12.1"
PORT = 30000

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
    server_socket.bind((HOST, PORT))
    server_socket.listen()
    client_socket, address = server_socket.accept()

    while 1 > 0:
    
        client_request = json.loads((client_socket.recv(1024).decode("utf8")))
        if client_request == "info" :
            server_answer = info
        elif client_request == "uptime" :
            server_answer = str(datetime.datetime.now() - uptime)
        elif client_request == "help" :
            server_answer = help_
        elif client_request == "close" :
            server_socket.close()
            client_socket.send(json.dumps("connetion terminated").encode("utf8"))
            break
        else:
            server_answer = "no response decicated"
        client_socket.send(json.dumps(server_answer).encode("utf8"))
        
    
